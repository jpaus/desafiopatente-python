from flask import Flask, jsonify

from patente import patente_bp

app = Flask(__name__)

app.register_blueprint(patente_bp, url_prefix="/patente")

if __name__ == "__main__":
    app.run(debug=True, port=5000)
    

