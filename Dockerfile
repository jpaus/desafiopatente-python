FROM python:3.8.10

RUN mkdir /patentepy

WORKDIR /patentepy

COPY . .

RUN pip3 install -r requirements.txt

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]