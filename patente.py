from flask import Blueprint, request, jsonify
from string import ascii_uppercase

patente_bp = Blueprint("patente", __name__)


@patente_bp.route("/patentetoid", methods=["POST"])
def patenteToId():
    patente = request.json.get("patente")
    if patente is None:
        return jsonify({"error": "No patente provided"}), 400
    if patente == "" or len(patente) != 7:
        return jsonify({"error": "Patente inválida, por favor ingrese una con formato AAAA000"}), 400

    patente_list = list(patente)
    patente_list_char = []
    patente_list_num = []
    for i in patente_list:
        if i.isalpha():
            patente_list_char.append(i)
        else:
            patente_list_num.append(i)

    if(len(patente_list_char) != 4 or len(patente_list_num) != 3 or any(char.islower() for char in patente_list_char)):
        return jsonify({"error": "Patente inválida, por favor ingrese una con formato AAAA000"}), 400

    patente_num = 0
    for index, i in enumerate(patente_list_char):
        patente_num += (ord(i)-65) * (26**(len(patente_list_char)-1-index ))

    patente_num_int = int("".join(patente_list_num))

    patente_id = 0
    patente_id = patente_num*1000 + patente_num_int + 1 

    return jsonify({"id": patente_id}), 200




@patente_bp.route("/idtopatente", methods=["POST"])
def idToPatente():
    patente_id = request.json.get("id")
    maxId = (26**4)*1000

    if patente_id is None:
        return jsonify({"error": "No id provided"}), 400

    if patente_id == "" or any(char.isalpha() for char in str(patente_id)):
        return jsonify({"error": "Ingrese una id valida entre 0 y "+str(maxId)}), 400

    patente_id = int(patente_id)-1
    patente_letters = ["A","A","A","A"]
    patente_letters_numbers = [0,0,0,0]
    patente_id_num_int = 0
    patente_id_num = patente_id % 1000

    if patente_id >= 1000:
        patente_id_num_int = patente_id // 1000
        
    if patente_id_num_int // (26**4)% 26 >= 1 or  patente_id < 0:
        
        return jsonify({"error": "Ingrese una id valida entre 0 y "+str(maxId)}), 400

    if patente_id_num_int >= 1:
        for i in range(len(patente_letters)):
            patente_letters_numbers[len(patente_letters)-1-i] = patente_id_num_int // (26**i) % 26
   

    for index, i in enumerate(patente_letters_numbers):
        patente_letters[index] = ascii_uppercase[i]

    patente_letters = "".join(patente_letters)
    patente_numbers = str(patente_id_num)
    patente_numbers = "0"*(3-len(patente_numbers)) + patente_numbers
    patente = patente_letters + patente_numbers

    return jsonify({"patente": patente}), 200

