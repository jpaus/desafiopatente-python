## How to try

```sh

  sh rundocker.sh

```
 
 or
   
```sh
    
    docker build -t desafiopatente:latest .
    docker run -i -t -d -p 5000:5000 desafiopatente:latest

```



## Example patentetoid
```sh
Post  http://localhost:5000/patente/patentetoid
body = {
  "patente":"ZZZZ999"
}
```

> output: 456976000

**

## Example idtopatente
```sh
Post  http://localhost:5000/patente/idtopatente
body = {
  "id": 456976000
}
```

> output: ZZZZ999


**


made with love :heart:
